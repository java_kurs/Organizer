package com.organizer.app;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created by T420 on 2017-07-27.
 */
@RunWith(MockitoJUnitRunner.class)
public class CalendarTest {

    @Spy
    List<Event> test = new ArrayList<>();
    Event e1 = new Event.EventBuilder("Kurs").eventDay(2017,2,13)
            .eventTime(10,30).minutesToRemind(30).eventNote("dupa").build();
    @Test
    public void addEvent() throws Exception {
        test.add(e1);
        assertThat(test).contains(e1);
    }

    @Test
    public void deleteEvent() throws Exception {
        addEvent();
        test.remove(0);
        assertThat(test).doesNotContain(e1);
    }

    @Test
    public void editEventName() throws Exception {
        addEvent();
        test.get(0).setName("Nope");
        assertThat(test.get(0).getName()).doesNotMatch("Kurs");
    }

    @Test
    public void editEventStart() throws Exception {
        addEvent();
        test.get(0).setStartTime(2017,2,23,2,32);
        assertThat(test.get(0)).isNotEqualTo(new DateTime(2017,2,13,10,30));
    }

    @Test
    public void editEventRemindTime() throws Exception {
        addEvent();
        test.get(0).setRemindTime(30);
        assertThat(test.get(0).getRemindTime())
                .isNotEqualTo(new DateTime(2017,2,13,10,30));

    }

    @Test
    public void editEventNote() throws Exception {
        addEvent();
        test.get(0).setNote("newnote");
        assertThat(test.get(0).getNote()).doesNotMatch("dupa");
    }

    @Test
    public void findEventByNameTest() throws Exception{
        addEvent();
        String name = "Kurs";
        int size = test.size();
        Event event = null;
        for (int i = 0; i < size; i++) {
            if (test.get(i).getName().equals(name)) {
                event = test.get(i);
            }
        }
        assertThat(event).isEqualTo(e1);
    }

}