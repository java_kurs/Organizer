package com.organizer.app;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by T420 on 2017-07-27.
 */
public class Calendar {
    private List<Event> eventList;

    public Calendar() {
        eventList = new ArrayList<>();
    }

    public List<Event> getEventList() {
        return eventList;
    }

    public void addEvent(Event event) {
        eventList.add(event);
    }

    public void deleteEvent(Event event) {
        eventList.remove(eventList.indexOf(event));
    }

    public void editEventName(Event event, String name) {
        eventList.get(eventList.indexOf(event)).setName(name);
    }

    public void editEventStart(Event event, int year, int month, int day, int hour, int minute) {
        eventList.get(eventList.indexOf(event)).setStartTime(year, month, day, hour, minute);
    }

    public void editEventRemindTime(Event event, int minutes) {
        eventList.get(eventList.indexOf(event)).setRemindTime(minutes);
    }

    public void editEventNote(Event event, String note) {
        eventList.get(eventList.indexOf(event)).setNote(note);
    }

    public Event findEventByName(String name) {
        int size = eventList.size();
        Event event = null;
        for (int i = 0; i < size; i++) {
            if (eventList.get(i).getName().equals(name)) {
                event = eventList.get(i);
            }
        }
        return event;
    }



}
