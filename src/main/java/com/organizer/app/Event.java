package com.organizer.app;

import org.joda.time.DateTime;

/**
 * Created by T420 on 2017-07-27.
 */
public class Event {
    private String name, note;
    private DateTime startTime, remindTime;

    private Event(EventBuilder builder) {
        this.name = builder.name;
        this.startTime = new DateTime(builder.year, builder.month, builder.day,
                builder.hour, builder.minute);
        this.remindTime = startTime.minusMinutes(builder.remindMinutes);
        this.note = builder.note;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(int year,int month,int day,int hour,int minute) {
        this.startTime = new DateTime(year,month,day,hour,minute);
    }

    public DateTime getRemindTime() {
        return remindTime;
    }

    public void setRemindTime(int minutes) {
        this.remindTime = this.remindTime = startTime.minusMinutes(minutes);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (!name.equals(event.name)) return false;
        if (!note.equals(event.note)) return false;
        if (!startTime.equals(event.startTime)) return false;
        return remindTime.equals(event.remindTime);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + note.hashCode();
        result = 31 * result + startTime.hashCode();
        result = 31 * result + remindTime.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Event{" +
                "name='" + name + '\'' +
                ", note='" + note + '\'' +
                ", startTime=" + startTime +
                ", remindTime=" + remindTime +
                '}';
    }

    public static class EventBuilder {
        private String name, note;
        private int year, month, day, hour, minute, remindMinutes;

        public EventBuilder(String name) {
            this.name = name;
        }

        public EventBuilder eventDay(int year, int month, int day) {
            this.year = year;
            this.month = month;
            this.day = day;
            return this;
        }

        public EventBuilder eventTime(int hour, int minute) {
            this.hour = hour;
            this.minute = minute;
            return this;
        }

        public EventBuilder eventNote(String note) {
            this.note = note;
            return this;
        }

        public EventBuilder minutesToRemind(int remindMinutes) {
            this.remindMinutes = remindMinutes;
            return this;
        }

        public Event build() {
            return new Event(this);
        }
    }
}
